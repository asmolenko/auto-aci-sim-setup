# Read pre-requisites section in the README file
# Make sure the current directory has "VMKeystrokes.ps1" file
# Importing the function with keystrokes
import-module .\VMKeystrokes.ps1 -force
#
# *** *** ***
# Do we need to set variables by a user? If yes, change the section below. Should we provide an option with choice  - pre-configured vs manual?
# *** *** ***  
#
# Pre-set variables
Set-variable -Name "vCenter" -Value "172.18.202.53"
Set-variable -Name "VM" -Value "AS-SIM-5.0-1k"
Set-variable -Name "FabricName" -Value "Fabric1"
Set-variable -Name "FabricID" -Value "1"
Set-variable -Name "ActiveControllers" -Value "1"
Set-variable -Name "PodID" -Value "1"
Set-variable -Name "StandbyController" -Value "NO"
Set-variable -Name "ControllerID" -Value "1"
Set-variable -Name "ControllerName" -Value "APIC1"
Set-variable -Name "TEP_Pool" -Value "10.0.0.0/16"
Set-variable -Name "InfraVLANID" -Value "4"
Set-variable -Name "Multicast_Pool" -Value "225.0.0.0/15"
Set-variable -Name "OOB_IPV6" -Value "N"
Set-variable -Name "OOB_IPV4" -Value "100.10.10.98/24"
Set-variable -Name "DG_IPV4" -Value "100.10.10.1"
Set-variable -Name "DuplexSpeed" -Value "auto"
Set-variable -Name "StrongPassword" -Value "N"
Set-variable -Name "Password" -Value "C1sco123!"
#
#  
"The following configuration will be pushed to the ACI Simulator:"
"The fabric name: " + $FabricName
"The fabric ID: " + $FabricID
"The number of active controllers in the fabric: " + $ActiveControllers
"The POD ID: " + $PodID
"Is this a standby controller: " + $StandbyController
"The controller ID: " + $ControllerID
"The controller name: " + $ControllerName
"Address pool for TEP addresses: " + $TEP_Pool
"The VLAN ID for infra network: " + $InfraVLANID
"Address pool for BD multicast addresses: " + $Multicast_Pool
"Enable IPv6 for Out of Band Mgmt interface: " + $OOB_IPV6
"The IPv4 address of Out of Band Mgmt interface: " + $OOB_IPV4
"The IPv4 address of the default gateway: " + $DG_IPV4
"The interface speed/duplex mode: " + $DuplexSpeed
"Enable strong password: " + $StrongPassword
"The admin user's password: " + $Password
Read-Host -Prompt "Press Enter to continue"
"Pushing confguration to the ACI Simulator..."
# Connecting to vCenter / ESXi 
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
$connection=Connect-VIServer -Server $vCenter -Protocol https -User 'administrator@vsphere.local' -Password 'cisco.123'
#
# *** *** ***
# Do we need to reset ACI SIM VM from this script? If yes, make sure the delay ~2 minutes is set after the VM was reset
# *** *** *** 
#
# Set Fabric Name
Set-VMKeystrokes -VMName $VM -StringInput $FabricName -ReturnCarriage $true
# Set Fabric ID
Set-VMKeystrokes -VMName $VM -StringInput $FabricID -ReturnCarriage $true
# Set the Number of Active Controllers
Set-VMKeystrokes -VMName $VM -StringInput $ActiveControllers -ReturnCarriage $true
# Set Pod ID
Set-VMKeystrokes -VMName $VM -StringInput $PodID -ReturnCarriage $true
# Set Standby Controller
Set-VMKeystrokes -VMName $VM -StringInput $StandbyController -ReturnCarriage $true
# Set Controller ID
Set-VMKeystrokes -VMName $VM -StringInput $ControllerID -ReturnCarriage $true
# Set Controller Name
Set-VMKeystrokes -VMName $VM -StringInput $ControllerName -ReturnCarriage $true
# Set TEP Pool
Set-VMKeystrokes -VMName $VM -StringInput $TEP_Pool -ReturnCarriage $true
# Set Infra VLAN ID
Set-VMKeystrokes -VMName $VM -StringInput $InfraVLANID -ReturnCarriage $true
# Set Multicast Pool
Set-VMKeystrokes -VMName $VM -StringInput $Multicast_Pool -ReturnCarriage $true
# Set OOB IPV6 Y/N
Set-VMKeystrokes -VMName $VM -StringInput $OOB_IPV6 -ReturnCarriage $true
# Set OOB IPV4 Address
Set-VMKeystrokes -VMName $VM -StringInput $OOB_IPV4 -ReturnCarriage $true
# Set Default Gateway IPV4 Address
Set-VMKeystrokes -VMName $VM -StringInput $DG_IPV4 -ReturnCarriage $true
# Set Speed / Duplex
Set-VMKeystrokes -VMName $VM -StringInput $DuplexSpeed -ReturnCarriage $true
# Enable Strong Password
Set-VMKeystrokes -VMName $VM -StringInput $StrongPassword -ReturnCarriage $true
# Set Password
Set-VMKeystrokes -VMName $VM -StringInput $Password -ReturnCarriage $true
Set-VMKeystrokes -VMName $VM -StringInput $Password -ReturnCarriage $true
# Do not edit configuration
Set-VMKeystrokes -VMName $VM -ReturnCarriage $true
