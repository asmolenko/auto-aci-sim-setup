# Auto ACI Sim Setup

This project provides an aid in automating the configuration of the ACI Simulator VM after the VM was reset or rebooted.

Prerequisites:
 - Powershell
 - Function https://github.com/lamw/vghetto-scripts/blob/master/powershell/VMKeystrokes.ps1
 - tba